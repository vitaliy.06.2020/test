package com.laba;

import java.util.Random;

public class Matrix {
    int n=0;
    int[][] matrix;


    public Matrix(int n) {
        this.n = n;
        this.matrix = new int[n][n];
    }

    public int getN() {
        return n;
    }

    public int[][] getMatrix() {
        return matrix;
    }
    public  int min(){
        int min = this.matrix[0][0];
        for(int i=0;i<this.n;i++){
            if (this.matrix[i][i]<min) min=this.matrix[i][i];
        }
        return  min;
    }
    public  void generate(){
        Random r = new Random();
        for (int i=0;i<n;i++){
            for (int j=0;j<n;j++){
                this.matrix[i][j]=r.nextInt(99)+1;
            }
        }
    }
    public void print(){
        for (int i=0;i<n;i++){
            for (int j=0;j<n;j++)
                System.out.printf("%3d ",this.matrix[i][j]);
            System.out.println();
        }
    }
}
